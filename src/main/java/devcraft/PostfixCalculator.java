package devcraft;

public class PostfixCalculator  {

	public double calc(String expression) {
    	
    	StackCalculator sc = new StackCalculator();
    	Tokenizer tokenizer = new Tokenizer(expression);
              
        while (tokenizer.hasNext()) {
            Object token = tokenizer.nextToken();

            if (isOperand(token)) {
				double number = (double) token;
				sc.push(number);
			}
            if (isOperator(token)) {
                char anOperator = (char) token;
                sc.apply(anOperator);
            }
        }
        return sc.result();
    }


	private boolean isOperator(Object token) {
		return token instanceof Character;
	}

	private boolean isOperand(Object token) {
		return token instanceof Double;
	}

}

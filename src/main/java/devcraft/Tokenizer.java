package devcraft;

class Tokenizer {

	private String expression;
	
	public Tokenizer(String expression) {
		this.expression = expression;
		populateOperandAndOperator(expression, numberTokenBuilder);
	}

	private Character populateOperandToken(String expression, StringBuilder tokenBuilder) {
	    Character c = expression.charAt(i);
	    while (Character.isDigit(c)) {
	        tokenBuilder.append(c);
	        i++;
	        if (i >= expression.length()) {
	            return null;
	        }
	        c = expression.charAt(i);
	    }
	    return c;
	}

	private String takeToken(StringBuilder tokenBuilder) {
	    String token = tokenBuilder.toString();
	    tokenBuilder.setLength(0);
	    return token;
	}

	private Double takeOperand(StringBuilder tokenBuilder) {
	    Double operand = null;
	    if (tokenBuilder.length() > 0) {
	        String token = takeToken(tokenBuilder);
	        operand = Double.parseDouble(token);
	    }
	    return operand;
	}

	private Character currOperator(Character c) {
	    if (c == null)
	        return null;
	    if (c == ',')
	        return null;
	    return c;
	}

	private void populateOperandAndOperator(String expression, StringBuilder tokenBuilder) {
	    while (i < expression.length() && (operand == null && operator == null)) {
	        Character followingChar = populateOperandToken(expression, tokenBuilder);
	        operand = takeOperand(tokenBuilder);
	        operator = currOperator(followingChar);
	        i++;
	    }
	}

	public boolean hasNext() {
		return operand != null || operator != null;
	}

	public Object nextToken() {
		Object token = null;
		if (operand != null) {
		    token = operand;
		    operand = null;
		} else if (operator != null) {
		    token = operator;
		    operator = null;
		}
		if (operand == null && operator == null)
		    populateOperandAndOperator(expression, numberTokenBuilder);
		return token;
	}

    private int i = 0;
    private Double operand;
    private Character operator;
    private StringBuilder numberTokenBuilder = new StringBuilder();
}
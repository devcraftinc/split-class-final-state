package devcraft;

import java.util.Stack;

class StackCalculator {
	private Stack<Double> operands = new Stack<>();
	
	public double result() {
		return operands.peek();
	}
	
	public void apply(char anOperator) {
		if (anOperator == '+') {
			operands.push(operands.pop() + operands.pop());
		} else if (anOperator == '-') {
			operands.push(-operands.pop() + operands.pop());
		} else if (anOperator == '*') {
			operands.push(operands.pop() * operands.pop());
		} else if (anOperator == '/') {
			operands.push(1.0 / operands.pop() * operands.pop());
		}
	}
	
	public void push(double number) {
		operands.push(number);
	}
}